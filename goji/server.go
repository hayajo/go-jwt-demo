package main

import (
	"flag"
	"io/ioutil"
	"net/http"
	"time"

	"gopkg.in/unrolled/render.v1"

	"github.com/auth0/go-jwt-middleware"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"github.com/zenazn/goji"
	"github.com/zenazn/goji/web"
	"github.com/zenazn/goji/web/middleware"
)

var (
	r          *render.Render
	privateKey []byte
	publicKey  []byte
)

func init() {
	privateKey, _ = ioutil.ReadFile("demo-key.pem")
	publicKey, _ = ioutil.ReadFile("demo-key.pem.pub")
	r = render.New(render.Options{})
}

func main() {
	flag.Set("bind", ":8080")

	apiRouter := web.New()
	apiRouter.Use(middleware.SubRouter)
	apiRouter.Get("/restricted", restrictedHandler)
	jwtMiddleware := jwtmiddleware.New(jwtmiddleware.Options{
		ValidationKeyGetter: func(token *jwt.Token) (interface{}, error) {
			return publicKey, nil
		},
	})
	apiRouter.Use(jwtMiddleware.Handler)

	rootRouter := web.New()
	rootRouter.Post("/authenticate", authenticateHandler)

	goji.Handle("/api/*", apiRouter)
	goji.Handle("/*", rootRouter)

	goji.Serve()
}

func restrictedHandler(rw http.ResponseWriter, req *http.Request) {
	r.JSON(rw, http.StatusOK, context.Get(req, "user"))
}

func authenticateHandler(c web.C, rw http.ResponseWriter, req *http.Request) {
	username := req.FormValue("username")
	password := req.FormValue("password")

	if !(username == "nanashi" && password == "hogehoge") {
		http.Error(rw, "Not Authorized", http.StatusUnauthorized)
		return
	}

	token := jwt.New(jwt.GetSigningMethod("RS256"))
	token.Claims["exp"] = time.Now().Add(time.Minute * 30).Unix()
	token.Claims["first_name"] = "taro"
	token.Claims["last_name"] = "nanashi"
	token.Claims["email"] = "taro.nanashi@example.com"
	token.Claims["id"] = 123

	tokenString, _ := token.SignedString(privateKey)

	rw.Write([]byte(tokenString))
}
