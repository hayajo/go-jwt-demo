package main

import (
	"io/ioutil"
	"net/http"
	"time"

	"github.com/codegangsta/negroni"
	"github.com/dgrijalva/jwt-go"
	"github.com/gorilla/context"
	"github.com/gorilla/mux"
	"gopkg.in/unrolled/render.v1"
)

var (
	r          *render.Render
	privateKey []byte
	publicKey  []byte
)

func init() {
	r = render.New(render.Options{})
	privateKey, _ = ioutil.ReadFile("demo-key.pem")
	publicKey, _ = ioutil.ReadFile("demo-key.pem.pub")
}

func main() {
	rootRouter := mux.NewRouter().StrictSlash(true)
	rootRouter.Methods("POST").Path("/authenticate").HandlerFunc(authenticateHandler)

	apiRouter := mux.NewRouter().StrictSlash(true)
	apiRouter.Methods("GET").Path("/api/restricted").HandlerFunc(restrictedHandler)

	rootRouter.PathPrefix("/api/{_:.+}").Handler(
		negroni.New(
			negroni.HandlerFunc(checkTokenMiddleware),
			negroni.Wrap(apiRouter),
		),
	)

	n := negroni.Classic()
	n.UseHandler(context.ClearHandler(rootRouter))
	n.Run(":8080")
}

func checkTokenMiddleware(rw http.ResponseWriter, req *http.Request, next http.HandlerFunc) {
	token, err := jwt.ParseFromRequest(req, func(token *jwt.Token) (interface{}, error) {
		return publicKey, nil
	})

	if err == nil && token.Valid {
		context.Set(req, "user", token)
		next(rw, req)
	} else {
		http.Error(rw, "Not Authorized", http.StatusUnauthorized)
	}
}

func authenticateHandler(rw http.ResponseWriter, req *http.Request) {
	username := req.FormValue("username")
	password := req.FormValue("password")

	if !(username == "nanashi" && password == "hogehoge") {
		http.Error(rw, "Not Authorized", http.StatusUnauthorized)
		return
	}

	token := jwt.New(jwt.GetSigningMethod("RS256"))
	token.Claims["exp"] = time.Now().Add(time.Minute * 30).Unix()
	token.Claims["first_name"] = "taro"
	token.Claims["last_name"] = "nanashi"
	token.Claims["email"] = "taro.nanashi@example.com"
	token.Claims["id"] = 123

	tokenString, _ := token.SignedString(privateKey)

	rw.Write([]byte(tokenString))
}

func restrictedHandler(rw http.ResponseWriter, req *http.Request) {
	r.JSON(rw, http.StatusOK, context.Get(req, "user"))
}
