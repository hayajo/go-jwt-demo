# go-jwtのデモ

サーバ実装は下記を参考にしました。

* [Cookies vs Tokens. Getting auth right with Angular.JS](https://auth0.com/blog/2014/01/07/angularjs-authentication-with-cookies-vs-token/)
* [Using JSON Web Token within Go web applications - YouTube](https://www.youtube.com/watch?v=eVlxuST7dCA)

## 試す

各ディレクトリで実行してください。

### キーペアの作成

    $ openssl genrsa > demo-key.pem
    $ openssl rsa -pubout < demo-key.pem > demo-key.pem.pub

### サーバー開始

    $ gom install
    $ gom exec go run server.go

### トークン取得

    $ TOKEN=$(curl -s -XPOST -d username=nanashi -d password=hogehoge localhost:8080/authenticate)

### 取得したトークンを使ってアクセス

    curl -H "Authorization: Bearer $TOKEN" localhost:8080/api/restricted

